<?php

use Drupal\Core\Url;

/**
 * Implements hook_page_attachments().
 */
function color_schema_ui_page_attachments(array &$attachments) {
  if (!\Drupal::service('router.admin_context')->isAdminRoute() && \Drupal::currentUser()->hasPermission('use color schema ui')) {
    $attachments['#attached']['library'][] = 'color_schema_ui/color_schema_ui';

    $template = [
      '#theme' => 'color_schema_ui',
    ];

    $attachments['#attached']['drupalSettings']['color_schema_ui'] = [
      'get_compiled_scss'          => Url::fromRoute('color_schema_ui.get_compiled_scss')
        ->toString(),
      'get_initial_colors'         => Url::fromRoute('color_schema_ui.get_initial_colors')
        ->toString(),
      'compile_scss_to_filesystem' => Url::fromRoute('color_schema_ui.compile_scss_to_filesystem')
        ->toString(),
      'html_template'              => \Drupal::service('renderer')
        ->renderPlain($template)
        ->__toString(),
      'color_machine_names'        => array_keys(\Drupal::service('serialization.yaml')::decode(\Drupal::config('color_schema_ui.settings')
        ->get('colors_definition_yaml'))),
      'current_route'              => \Drupal::routeMatch()->getRouteName(),
    ];
  }

  if (!\Drupal::service('router.admin_context')->isAdminRoute()) {
    $attachments['#attached']['library'][] = 'color_schema_ui/theme-assets';
  }
}

/**
 * Implements hook_css_alter().
 */
function color_schema_ui_css_alter(&$css, \Drupal\Core\Asset\AttachedAssetsInterface $assets)
{
  $pathToCSS = drupal_get_path('module', 'color_schema_ui') . '/../../../sites/default/files/color_schema_ui.css';

  if (!file_exists($pathToCSS)) {
    \Drupal::logger('color_schema_ui')->error('No color schema ui CSS file found in the following path: ' . $pathToCSS . ' Run `drush color_schema_ui:compile` (csuc) to generate it.');
  }

  if (!\Drupal::service('router.admin_context')->isAdminRoute()) {
    $librariesKey = drupal_get_path('module', 'color_schema_ui') . '/../../../sites/default/files/color_schema_ui.css';
    if (isset($css[$librariesKey])) {
      $css[$librariesKey]['group'] = 200;
    }
  }
}

/**
 * Implements hook_theme().
 */
function color_schema_ui_theme($existing, $type, $theme, $path) {
  return [
    'color_schema_ui' => [
      'variables' => [
        'colors' => \Drupal::service('serialization.yaml')::decode(\Drupal::config('color_schema_ui.settings')->get('colors_definition_yaml'))
      ],
    ],
  ];
}
