<?php

namespace Drupal\color_schema_ui;

class SCSSColorHandler {

  public function replaceColors(string $scss, array $colorsToReplace): string {
    foreach ($colorsToReplace as $colorName => $colorValue) {
      $colorName = $this->replaceColor($colorName);
      $scss = \preg_replace('/\$' . $colorName . ':.*;/', '$' . $colorName . ': ' . $colorValue . ';', $scss);
    }
    return $scss;
  }

  public function getInitialColors(string $scss): array {
    $initialColors = [];
    $scssLines = preg_split('/$\R?^/m', $scss);
    foreach ($scssLines as $scssLine) {
      // @var $nameAndColor
      //   See: https://regex101.com/r/lRhg0t/5
      $nameAndColor = '/\$(?\'variable_name\'[a-zA-Z0-9-_]+)\:\s(?\'color_val\'.+?(?=[\s|;]))(?>\s!default)?;/';
      $isColor = preg_match($nameAndColor, $scssLine, $matches);

      if ($isColor && isset($matches['color_val'])
          && $this->isSupportedColorValue(trim($matches['color_val']))
      ) {
        $initialColors[$matches['variable_name']] = trim($matches['color_val']);
      }
    }
    return $initialColors;
  }

  public function replaceColor(string $colorName, $reverse = FALSE): string {
    if ($reverse) {
      $colorName = str_replace('-', '_', $colorName);
    }
    else {
      $colorName = str_replace('_', '-', $colorName);
    }
    return $colorName;
  }

  public function isSupportedColorValue(string $value): bool {
    $X = FALSE;
    if (preg_match('/^(#(?:[0-9a-f]{2}){2,4}|#[0-9a-f]{3}|(?:rgba?|hsla?)\((?:\d+%?(?:deg|rad|grad|turn)?(?:,|\s)+){2,3}[\s\/]*[\d\.]+%?\))$/i', $value)) {
      return TRUE;
    }
    return FALSE;
  }
}
