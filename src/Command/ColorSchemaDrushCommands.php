<?php

namespace Drupal\color_schema_ui\Command;

use Drupal\color_schema_ui\SCSSCompilerFacade;
use Drupal\Core\File\FileSystemInterface;
use Drush\Commands\DrushCommands;
use Drush\Exceptions\UserAbortException;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;



/**
 * Defines drush commands to manage the demo content.
 */
class ColorSchemaDrushCommands extends DrushCommands {

  /**
   * @var SCSSCompilerFacade
   */
  private $compiler;

  public function __construct(SCSSCompilerFacade $compiler) {
    parent::__construct();
    $this->compiler = $compiler;
  }

  /**
   * Deletes and regenerates the demo content.
   *
   * @command color_schema_ui:compile
   * @aliases csuc
   */
  public function compile() {
    $this->compiler->compileSCSSToFilesystem();
  }

  /**
   *
   * Migrates a existing color schema ui config. The files color_schema_ui.scss and color_schema_ui.css in sites/default/files will be replaced.
   *
   * @command color_schema_ui:migrate
   * @param String|null $color_schema_ui_scss Mirgrate from custom color_schema_ui template.
   *
   * @throws FileNotFoundException
   * @throws \Drush\Exceptions\UserAbortException
   * @throws \Exception
   */
  public function migrate($color_schema_ui_scss = null) {
    if (!$color_schema_ui_scss) {
      $theme = \Drupal::theme()->getActiveTheme();
      $color_schema_ui_scss = DRUPAL_ROOT . '/' . $theme->getPath() .'/source/sass/color_schema_ui.scss';
      if (!file_exists($color_schema_ui_scss)) {
        throw new FileNotFoundException("Could not find a color schema template at " . $color_schema_ui_scss);
      }
    }
    $this->output()->writeln("Found update source: ");
    $this->output()->writeln(" " . $color_schema_ui_scss);

    $colorSchemaScssPath = $this->compiler->getScssPath();
    if ($colorSchemaScssPath) {
      $previousColors = $this->compiler->getInitialColors($colorSchemaScssPath);
      $this->output()->writeln("Found color config: ");
      $this->exportColors();
      $this->output()->writeln("at destination: ");
      $this->output()->writeln(" " . $colorSchemaScssPath);
    }

    if ($this->io()->confirm("Destination file will be replaced. Continue?")) {
      $colorSchema = $this->compiler->getInitialColors($color_schema_ui_scss);
      foreach ($colorSchema as $colorName => $value) {
        if(isset($previousColors[$colorName])) {
          $colorSchema[$colorName] = $previousColors[$colorName];
        }
      }

      // Move new template to destinnation.
      if (!\Drupal::service('file_system')->copy($color_schema_ui_scss, $colorSchemaScssPath, FileSystemInterface::EXISTS_REPLACE)) {
        throw new \Exception('Could not upgrade your SCSS file at ' . $colorSchemaScssPath);
      }

      // Rebuild
      $this->compiler->compileSCSSToFilesystem($colorSchema);
      $this->output()->writeln("Migrated color Schema UI.");
    }
    else {
      throw new UserAbortException();
    }
  }

  /**
   * Exports current color variables.
   *
   * @command color_schema_ui:exportColors
   */
  public function exportColors() {
    $colorSchema = $this->compiler->getInitialColors();
    $this->output()->writeln(rtrim(str_replace("Array\n", '', print_r($colorSchema, 1)), "\n"));
  }

}
