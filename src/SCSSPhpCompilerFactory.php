<?php

namespace Drupal\color_schema_ui;

use ScssPhp\ScssPhp\Compiler;


class SCSSPhpCompilerFactory {

  public function create(): Compiler {
    return new Compiler();
  }

}
