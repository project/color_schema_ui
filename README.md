CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The Color Schema UI module extends Drupal by functionality for changing your sites color schema via a CSS selectors 
related color picker. So you are able to define a handful of base color zones (e.g. site background color, footer
background color, content font color etc.) and let specific user roles define them easily. You will get a 
[color picker](https://vanilla-picker.js.org/), so non-developer users can change the colors of your site. Colors will 
be compiled into a SCSS (for defining the color variables) and CSS file in your site's files folder.

Place a color_schema_ui.scss file into your active theme for inital color settings. If there is none, there is a default
SCSS file in the module.

REQUIREMENTS
------------

This module requires no Drupal modules outside of Drupal core. Libraries on which this module is based:
* [scssphp](https://github.com/scssphp/scssphp): SCSS compiler written in PHP
* [vanilla-picker](https://vanilla-picker.js.org/): A simple, easy to use vanilla JS (no dependencies) color picker 
  with alpha selection.
* [invert-color](https://github.com/onury/invert-color): Generates inverted (opposite) version of the given color. (<1KB)

See all JavaScript library dependencies in `color_schema_ui/js/package.json`. All PHP library
dependencies are contained in `color_schema_ui/composer.json`.

INSTALLATION
------------

 * Install the Color Schema UI module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.
   
If you have made any JavaScript code changes, you must re-compile the JavaScript code.

1. Switch to the folder cd `color_schema/js`
2. Make sure NPM and NVM are installed
3. Run `nvm use` 
4. Run `npm install` 
5. Run `npm run build` (compiles the assets for a production environment)

To run dev more you can run run

 `npm run build-dev`

Which will start a webpack and compile the assets with sourcemaps.


UPGRADING
---------

Assuming you have a enabled color_schema_ui module running. 

* Update code of color_schema_ui module and your theme 
* Run the drush command `drush color_schema_ui:migrate` this will  
  * Look for your previous docroot/sites/default/files/color_schema_ui.scss
  * Extract color variables from this file
  * Look for a template at nrw_base_theme/source/sass/color_schema_ui.scss
  * Rebuild docroot/sites/default/files/color_schema_ui.(s)css with the extracted colors and the new template
  

EXPORT COLORS
=================== 

To export colors use `drush color_schema_ui:exportColors`.

This will read colors from `docroot/sites/default/files/color_schema_ui.scss` and display them in console. e.g: 

```
$ drush color_schema_ui:exportColors
(
    [site-background-color] => rgba(109,215,50,1)
    [peripheral-color-primary] => #233755
    [font-contrast-color-primary] => rgb(234,239,246)
    [peripheral-color-secondary] => #3C506E
    [font-contrast-color-secondary] => rgba(193,21,229,1)
    [header-background-color] => rgb(234, 239, 246)
    [content-background-color] => rgba(139,246,207,1)
    [paragraphs-header-background-color] => rgba(91,130,184,1)
    [font-content-color] => #233755
    [font-header-color] => rgba(220,221,192,1)
    [font-footer-color] => rgba(28,176,91,1)
)
```

If something went wrong you can re-enable the module and use these color values in the UI restore your color schema. 



CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > System > Color Schema UI to edit the possible colors. After the
       colors are configured, mind the SCSS definitions to propagate color picker settings to the theme.
    3. Visit any node and pick your color by the local task tab menu item. Color Schema UI is visible on nodes only.

If you want to use brighter Primary/Secondary colors you can add the following at the configuration page (admin/color-schema-ui/settings) to enable non-white contrast colors: 

```
font_contrast_color_primary: 'Primary contrast'
peripheral_color_secondary: Peripheral color secondary
```




Known Issues
---------------

```
# Does not work with some release of quickedit < 8.7.10
drush pm-uninstall quickedit -y
drush en -y color_schema_ui
```


MAINTAINERS
-----------

 * Peter Majmesku - https://www.drupal.org/u/peter-majmesku

Supporting organization:

 * publicplan GmbH - https://www.drupal.org/publicplan-gmbh
